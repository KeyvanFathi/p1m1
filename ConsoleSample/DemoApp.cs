﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Xml;
using System.Xml.Linq;
using System.Reflection;

namespace DemoApp
{
    public abstract class BaseEntity
    {
        public string Id { get; protected set; }
        public virtual Address Address { get; set; }
    }

    public interface IRepository<T> where T : BaseEntity
    {
        string Save();
        void Delete(string key);
        bool Equals(object other);
    }

    #region Address
     public class Address 
    {
        //"4875 Sun Tail", "Queen Creek", "TX", "38452"
        public string Street { get; set; }
        public string District { get; set; }
        public string City { get; set; }
        public string ZipCode { get; set; }

        public Address() { }

        public Address(string street, string district, string city, string zipCode)
        {
            this.Street = street;
            this.District = district;
            this.City = city;
            this.ZipCode = zipCode;
        }
        public override bool Equals(object other)
        {
            var toCompareWith = other as Address;
            if (toCompareWith == null)
                return false;
            return (Street == toCompareWith.Street && District == toCompareWith.District && City == toCompareWith.City && ZipCode == toCompareWith.ZipCode);
        }

        public override int GetHashCode()
        {
            return this.Street.GetHashCode() ^
                   this.District.GetHashCode() ^
                   this.City.GetHashCode() ^
                   this.ZipCode.GetHashCode();
        }
    }
    #endregion

    #region Company

    public class Company : BaseEntity, IRepository<BaseEntity>
    {
        public string Name { get; set; }

        public Company() { }

        public Company(string name, Address address) {
            this.Name = name;
            this.Address = address;
        }

        public void Delete(string key)
        {
            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Company.xml");
            xdoc.Root.Elements("Company").Where(d => (string)d.Element("Id") == key).Remove();
            xdoc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Company.xml");
            this.Id = null;
        }

        public static Company Find(string key)
        {
            Company result = null;
            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Company.xml");
            var doc = xdoc.Root.Elements("Company").Where(d => (string)d.Element("Id") == key).FirstOrDefault();
            if (doc != null)
            {
                result = new Company();
                result.Id = (string)doc.Element("Id");
                result.Name = (string)doc.Element("Name");

                Address address = new Address();
                address.Street = (string)doc.Element("Address").Element("Street").Value;
                address.City = (string)doc.Element("Address").Element("City").Value;
                address.District = (string)doc.Element("Address").Element("District").Value;
                address.ZipCode = (string)doc.Element("Address").Element("ZipCode").Value;

                result.Address = address;
            }
            return result;
        }

        public string Save()
        {
            Id = new Random().Next().ToString();
            //Save XML
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.OmitXmlDeclaration = true;
            xws.Indent = true;

            using (XmlWriter xw = XmlWriter.Create(sb, xws))
            {
                XDocument doc = new XDocument(
                    new XElement("Companies",
                    new XElement(this.GetType().Name ,
                        new XElement(nameof(this.Id), this.Id),
                        new XElement(nameof(this.Name), this.Name),
                         new XElement(nameof(this.Address),
                            new XElement(nameof(this.Address.Street), this.Address.Street),
                            new XElement(nameof(this.Address.District), this.Address.District),
                            new XElement(nameof(this.Address.City), this.Address.City),
                            new XElement(nameof(this.Address.ZipCode), this.Address.ZipCode)
                        )
                        )
                    )
                );
                doc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Company.xml");
            }
            return Id;
        }

        public override bool Equals(object other)
        {
            var toCompareWith = other as Company;
            if (toCompareWith == null)
                return false;
            return this.Name == toCompareWith.Name;
        }

        public override int GetHashCode()
        {
            return this.Name.GetHashCode();
        }
    }
    #endregion

    #region Person
   
    public class Person: BaseEntity, IRepository<BaseEntity>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        //"Bill", "Smith", address
        public Person() {  }
        public Person(string firstName, string lastName, Address address) { 
            
            this.FirstName = firstName;
            this.LastName = lastName;
            this.Address = address;
        }

        public void Delete(string key)
        {
            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Person.xml");
            xdoc.Root.Elements("Person").Where(d => (string)d.Element("Id") == key).Remove();
            xdoc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Person.xml");
            this.Id = null;
        }

        public static Person Find(string key)
        {
            Person result = null;
            XDocument xdoc = XDocument.Load(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) + "Person.xml");
            var doc = xdoc.Root.Elements("Person").Where(d => (string)d.Element("Id") == key).FirstOrDefault();
            if (doc != null)      { 
            result = new Person();
            result.Id = (string)doc.Element("Id");
            result.FirstName = (string)doc.Element("FirstName");
            result.LastName = (string)doc.Element("LastName");
            //
            Address address = new Address();
            address.Street = (string)doc.Element("Address").Element("Street").Value;
            address.City = (string)doc.Element("Address").Element("City").Value;
            address.District = (string)doc.Element("Address").Element("District").Value;
            address.ZipCode = (string)doc.Element("Address").Element("ZipCode").Value;
            //
            result.GetType().GetProperty("Address").SetValue(result, address, null);
            }
            return result;
        }

        public string Save()
        {
            Id = new Random().Next().ToString();
            //Save XML
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings xws = new XmlWriterSettings();
            xws.OmitXmlDeclaration = true;
            xws.Indent = true;

            using (XmlWriter xw = XmlWriter.Create(sb, xws))
            {
                XDocument doc = new XDocument(
                     new XElement(this.GetType().Name+"s",
                        new XElement(this.GetType().Name,
                            new XElement(nameof(this.Id), this.Id), 
                            new XElement(nameof(this.FirstName), this.FirstName),
                            new XElement(nameof(this.LastName), this.LastName),
                             new XElement(nameof(this.Address),
                                new XElement(nameof(this.Address.Street), this.Address.Street),
                                new XElement(nameof(this.Address.District), this.Address.District),
                                new XElement(nameof(this.Address.City), this.Address.City),
                                new XElement(nameof(this.Address.ZipCode), this.Address.ZipCode)
                            )
                         )
                    )
                );
                doc.Save(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location)+"Person.xml");
            }
            return Id;
        }

        public override bool Equals(object other)
        {
            var toCompareWith = other as Person;
            if (toCompareWith == null)
                return false;
            return this.FirstName == toCompareWith.FirstName &&
                this.LastName == toCompareWith.LastName;
        }

        public override int GetHashCode()
        {
            return this.FirstName.GetHashCode() ^
                   this.LastName.GetHashCode();
        }
    }
    #endregion
}
   